from django.db import models
from django.contrib.auth.models import User, BaseUserManager
from address.models import AddressField
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.db.models import signals
from django.dispatch import receiver
from authtools.models import AbstractEmailUser
from django.utils import timezone

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.contrib.auth.tokens import default_token_generator
import urllib
from email.MIMEImage import MIMEImage

CURRENCIES = [('AR$', 'Peso Argentino'), ('US$', 'US Dollar'), ('EUR', 'Euro')]
REWARD_TYPE = [('DISCOUNT', 'Descuento'), ('VOUCHER', 'Voucher'), ('PHYSICAL', 'Fisico')]
REWARD_STATUS = [('PENDIENTE','pendiente'), ('CANCELADO','cancelado'), ('CANJEADO','canjeado'), ('VENCIDO','vencido')]


class UserManager(BaseUserManager):
    def create(self, email, password='', is_admin=False, **kwargs):
        email = self.normalize_email(email)
        user = self.model(email=email, is_admin=is_admin, is_active=True, **kwargs)
        user.set_password(password)
        user.save()
        return user

    def create_user(self, email=None, password=None, **kwargs):
        return self.create(email, password, False, **kwargs)

    def create_superuser(self, email, password, **kwargs):
        return self.create(email, password, True, **kwargs)

class SingleUser(AbstractEmailUser):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    email_is_verified = models.BooleanField(default=False)
    is_company = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False, editable=False)
    is_super_admin = models.BooleanField(default=False, editable=False)
    first_name = models.CharField(_('first_name'), max_length=255, null=True)
    last_name = models.CharField(_('last_name'), max_length=255, null=True)
    phone = models.CharField(_('phone'), max_length=255, null=True)
    address = models.CharField(_('address'), max_length=255, null=True)
    city = models.CharField(_('city'), max_length=255, null=True)
    country = models.CharField(_('country'), max_length=255, null=True)
    state = models.CharField(_('state'), max_length=255, null=True)

    offset = models.SmallIntegerField('timezone offset in minutes', default=0)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    objects = UserManager()

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def get_short_name(self):
        return self.email

    def __unicode__(self):
        return self.email

    def profile(self):
        return self.profile
