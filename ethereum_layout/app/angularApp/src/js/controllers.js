angular.module('angularApp.controllers', ['angularApp.services', 'ngToast', 'ui.bootstrap.datetimepicker'])
.controller('HomeCtrl', function($window, $http, $scope, $resource, AuthService, $location, modalService) {
  $scope.indexClass = 'home';
  // This is the example
  SimpleStorage.set(5).then(function(value) {
      SimpleStorage.get().then(function(value) {
          var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'partials/modal.html'
          };
          var modalOptions = {
                    closeButtonText: 'Close',
                    actionButtonText: 'OK',
                    headerText: 'Congratulations. The smart contract is alive!',
                    bodyText: 'The value from the Smart contract is '+value.toNumber()
          };
          modalService.showModal(modalDefaults, modalOptions);
      });
  });
})
.controller('UserCtrl', function ($rootScope, $scope, $http, $window, $location, AuthService) {
      $scope.user = {email: '', password: ''};
      $scope.isAuthenticated = false;
      $scope.isRegister = false;
      $scope.welcome = '';
      $scope.message = '';

      $rootScope.$on('event:social-sign-in-success', function(event, userDetails){
          console.log(userDetails);
      });

      $scope.selected = 'None';
      $scope.menuOptions = [
          ['Mi cuenta', function ($itemScope) {
              $location.path("/accounts");
          }],
          ['Actualizar Contraseña', function ($itemScope) {
              $location.path("/password");
          }],
          ['Salir', function ($itemScope) {
              $scope.logout();
              $scope.indexClass = '';
              $location.path('/login');
          }]
      ];

      $scope.has_social = function(social) {
          var social_exists = false;
          if ($scope.profile) {
            console.log($scope.profile.social_accounts);
            $scope.profile.social_accounts.forEach(function(social_network) {
                  if (social_network.social === social) {
                      social_exists = true;
                  }
            });
            return social_exists;
          }
          return true;
      }

      $scope.login = function () {
          AuthService.login($scope.user)
          .then(function (data) {
            $window.sessionStorage.token = data.data.auth_token;
            $scope.isAuthenticated = true;
            $scope.indexClass = 'home';
            $location.path("/");
            location.reload();
          }, function (data) {
            // Erase the token if the user fails to log in
            // delete $window.sessionStorage.token;
            $scope.isAuthenticated = false;
            $scope.indexClass = '';

            // Handle login errors here
            $scope.error = data.data.non_field_errors[0];
            $scope.welcome = '';
          });
      };

      $scope.register = function () {
          AuthService.register($scope.user)
          .then(function (data, status, headers, config) {
            $location.path("/login");
          }, function (data) {
            // Erase the token if the user fails to log in
            delete $window.sessionStorage.token;
            $scope.isAuthenticated = false;

            // Handle login errors here
            $scope.error = 'Error: Invalid user or password';
            $scope.welcome = '';
          });
      };

      $scope.logout = function () {
        $scope.welcome = '';
        $scope.message = '';
        $scope.isAuthenticated = false;
        delete $window.sessionStorage.token;
      };

      $scope.callRestricted = function () {
        AuthService.me()
        .then(function (data) {
          $scope.isAuthenticated = true;
          $scope.profile = data.data;
          $scope.indexClass = 'home';
        }, function (data) {
          $scope.isAuthenticated = false;
          $scope.indexClass = '';
          $location.path("/login");
        });
      };
      $scope.callRestricted()

    })
.controller('AccountCtrl', function($scope, AuthService, ngToast) {
  $scope.update = function update(){
      AuthService.update_me($scope.profile);
      ngToast.create({
        className: 'success',
        content: 'Updated',
        dismissButton: true
      });
  }
});
