angular.module('angularApp.services', ['angularApp.config', 'ngResource', 'ui.bootstrap'])
.factory('AuthService', function($window, $http, backend){
    return {
      login: function(user){
          return $http.post(backend + '/auth/login/', user);
      },
      register: function(user) {
          return $http.post(backend + '/auth/register/', user)
      },
      me: function() {
          return $http({url: backend + '/auth/me/', method: 'GET', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }})
      },
      social_account: function(social) {
          return $http({url: backend + '/social_accounts/add',  method: 'PUT', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: social})
      },
      update_me: function(profile){
          return $http({url: backend + '/auth/me/',  method: 'PUT', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: profile})
      },
      update_password: function(passwords){
          return $http({url: backend + '/companies/change_password',  method: 'POST', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: passwords})
      },
      rewards_to_exchange: function() {
          return $http({url: backend + '/rewardsxuser', method: 'GET', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }})
      },
      update_reward_exchange: function(item){
          return $http({url: backend + '/rewardsxuser/'+item.pk+'/commit',  method: 'POST', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }})
      },
      subscribers_list: function(){
          return $http({url: backend + '/subscription',  method: 'GET', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }})
      },
      update_subscription: function(subscription){
          return $http({url: backend + '/subscription/'+subscription.pk+'/update_points',  method: 'POST', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: subscription})
      },
      add_reward: function(reward){
          return $http({url: backend + '/rewards/add',  method: 'PUT', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: reward})
      },
      update_reward: function(reward){
          return $http({url: backend + '/rewards/'+reward.pk+'/update',  method: 'POST', headers: { 'Authorization': 'Token '+ $window.sessionStorage.token }, data: reward})
      },

    }
});
angular.module('angularApp.services').service('modalService', ['$modal',
    function ($modal, templateUrl) {

        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'partials/modal.html'
        };

        var modalOptions = {
            closeButtonText: 'Close',
            actionButtonText: 'OK',
            headerText: 'Proceed?',
            bodyText: 'Perform this action?'
        };

        this.showModal = function (customModalDefaults, customModalOptions) {
            if (!customModalDefaults) customModalDefaults = {};
            customModalDefaults.backdrop = 'static';
            return this.show(customModalDefaults, customModalOptions);
        };

        this.show = function (customModalDefaults, customModalOptions) {
            //Create temp objects to work with since we're in a singleton service
            var tempModalDefaults = {};
            var tempModalOptions = {};

            //Map angular-ui modal custom defaults to modal defaults defined in service
            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            //Map modal.html $scope custom properties to defaults defined in service
            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function ($scope, $modalInstance) {
                    $scope.modalOptions = tempModalOptions;
                    $scope.modalOptions.points = 0;
                    $scope.modalOptions.ok = function (result) {
                        $modalInstance.close($scope);
                    };
                    $scope.modalOptions.close = function (result) {
                        $modalInstance.dismiss('cancel');
                    };
                }
            }

            return $modal.open(tempModalDefaults).result;
        };

    }]);