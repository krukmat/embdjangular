Como el server no funciona correctamente para actualizarlo utilizando el comando fab server update.
Se debe activar el entorno virtual python
y luego correr el comando de gunicorn de start todo desde ssh en el server mismo.
1) source activate
2)cd /var/www/myPoints
3) gunicorn --daemon --pid ~/gunicorn.pid --bind 0.0.0.0:8000 --worker-class gevent --timeout 300 --workers 4 website.wsgi:application