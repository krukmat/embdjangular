#!/bin/sh
add-apt-repository -y ppa:ethereum/ethereum
apt-get update
apt-get install -y ethereum

npm -g install sync-exec
npm -g install embark
npm -g install ethereumjs-testrpc