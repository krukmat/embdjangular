angular.module('angularApp.controllers', ['angularApp.services', 'ngToast', 'ja.qr'])
.controller('HomeCtrl', function($window, $http, $scope, $resource, CompanyService, CompanySearch, AuthService, $location) {
   $scope.indexClass = 'home';
   // TODO: Usar filter
   function search(pk, companies){
      for (var i=0; i < companies.length; i++) {
          if (companies[i].company.pk === pk) {
              return true;
          }
      }
      return false;
   }
   function number_available_rewards(rewards, points_pending) {
      let final_rewards = [];
      angular.forEach(rewards, function (reward, index) {
           if (points_pending>=reward.points)
              final_rewards.push(reward);
      });
      return final_rewards.length;
   }

   $scope.reward_available = function(company){
      return $window.company_new_rewards.indexOf(company)>=0;
   }

   $scope.$watch(
              function(scope) { return scope.search },
              function(newValue, oldValue) {
                  var company;
                  if(newValue) {
                    company = CompanySearch.search({'name_contains': newValue}).get().$promise;
                  } else {
                    // devolver todos los valores
                    company = CompanyService.get().$promise;
                  }
                  company.then(function(_response) {
                      $scope.companies = _response.results;
                      $scope.filtered_companies = [];
                      // Show only the companies which are not subscribed
                      $scope.companies.forEach(function(company) {
                          if (search(company.pk, $scope.profile.subscriptions) === false) {
                              $scope.filtered_companies.push(company)
                          }
                      });
                  });
              }
   );
   AuthService.me()
        .then(function (data) {
            $scope.profile = data.data;
            $scope.isAuthenticated = true;
            // TODO: Codigo duplicado. Refactor
            var promise = CompanyService.get().$promise;
            promise.then(function(_response) {
                $scope.companies = _response.results;
                $scope.filtered_companies = [];
                // Show only the companies which are not subscribed
                $scope.companies.forEach(function(company) {
                    if (search(company.pk, $scope.profile.subscriptions) === false) {
                        $scope.filtered_companies.push(company)
                    }
                });
            });
        }, function (data) {
            $scope.isAuthenticated = false;
            $location.path("/login");
        });
})
.controller('MyRewardsCtrl', function($window, $http, $scope, $resource, CompanyService, CompanySearch, AuthService, $location) {
   $scope.indexClass = 'home';
   AuthService.me()
        .then(function (data) {
            $scope.profile = data.data;
            $scope.isAuthenticated = true;
        }, function (data) {
            $scope.isAuthenticated = false;
            $location.path("/login");
        });
})
.controller('CompaniesListCtrl', function($window, $http, $scope, $resource, CompanyService, CompanySearch, AuthService, CategoryService, $location) {
   $scope.indexClass = 'home';
   function search(pk, companies){
    for (var i=0; i < companies.length; i++) {
        if (companies[i].company.pk === pk) {
            return true;
        }
    }
    return false;
   }
   $scope.$watch(
              function(scope) { return scope.category_selected },
              function(newValue, oldValue) {
                  console.log(newValue.description);
                  // filter by category
                  var company = CompanySearch.search({'category_contains': newValue.description}).get().$promise;
                    company.then(function(_response) {
                      $scope.companies = _response.results;
                      $scope.filtered_companies = [];
                      // Show only the companies which are not subscribed
                      $scope.companies.forEach(function(company) {
                          if (search(company.pk, $scope.profile.subscriptions) === false) {
                              $scope.filtered_companies.push(company)
                          }
                      });
                    });
              }
   );
   AuthService.me()
        .then(function (data) {
            $scope.profile = data.data;
            $scope.isAuthenticated = true;
            // category list
            var category_list = CategoryService.get().$promise;
            category_list.then(function(_category_list_response) {
                $scope.category_list = _category_list_response.results;
                var promise = CompanyService.get().$promise;
                promise.then(function(_response) {
                    $scope.companies = _response.results;
                    $scope.filtered_companies = [];
                    // Show only the companies which are not subscribed
                    $scope.companies.forEach(function(company) {
                        if (search(company.pk, $scope.profile.subscriptions) === false) {
                            $scope.filtered_companies.push(company)
                        }
                    });
                });
            });
        }, function (data) {
            $scope.isAuthenticated = false;
            $location.path("/login");
        });
})
.controller('CompanyCtrl', function($window, $http, $scope, $resource,  $stateParams, CompanyService, AuthService, SubscriptionService, PointsService, $location, modalService, ngToast) {
  $scope.indexClass = 'home';
  //TODO: Refactor this. Move it into service. Handle only the data here
  function search(pk, companies){
    for (var i=0; i < companies.length; i++) {
        if (companies[i].company.pk === pk) {
            return companies[i];
        }
    }
    return null;
   }
  AuthService.me()
        .then(function (data) {
            $scope.profile = data.data;
            $scope.isAuthenticated = true;
            $scope.is_subscribed = search(parseInt($stateParams.id), $scope.profile.subscriptions);

            var promise = CompanyService.get({id: $stateParams.id}).$promise;
            promise.then(function(_response) {
                    var final_rewards = [];
                    let available_rewards = [];
                    $scope.object = _response;
                    // filtrar los premios que son canjeables
                    angular.forEach(_response.rewards, function (reward, index) {
                        console.log($scope.is_subscribed);
                        console.log(reward.points);
                        if (($scope.is_subscribed!=null) && ($scope.is_subscribed.points_pending>=reward.points)) {
                            final_rewards.push(reward);
                        } else
                            available_rewards.push(reward);
                    });
                    $scope.rewards = final_rewards;
                    $scope.available_rewards = available_rewards;
            });
        }, function (data) {
            $scope.isAuthenticated = false;
            $location.path("/login");
        });

  $scope.subscribe = function(company) {

        var modalOptions = {
            closeButtonText: 'Cancelar',
            actionButtonText: 'Confirmar',
            headerText: 'Confirma suscripcion?',
            bodyText: 'Confirmacion'
        };

        modalService.showModal({}, modalOptions).then(function (result) {

            SubscriptionService.subscribe(company).then(function (data) {
                $scope.is_subscribed = true;
            }, function (data) {
                $scope.error = data.non_field_errors[0];
            });

        });


  },
  $scope.redeemPoints = function(company, itemId, event){
      var modalOptions = {
            closeButtonText: 'Cancelar',
            actionButtonText: 'Confirmar',
            headerText: '¿Confirmar?',
            bodyText: 'Está por canjear '+itemId.points+' puntos por el premio '+itemId.name+', ¿confirma?'
      };
      event.stopPropagation();
      modalService.showModal({}, modalOptions).then(function (result) {

                PointsService.redeem(company, itemId).then(function (data, status, headers, config) {
                  $scope.redeem_code = data.data.code;
                  $scope.is_subscribed.points_pending = data.data.points_pending;
                  itemId.stock =  data.data.stock;
                  ngToast.create({dismissButton: true, timeout: 20000, content: 'Premios canjeados correctamente'});
                }, function(result){
                    ngToast.create({
                                    className: 'error',
                                    dismissButton: true,
                                    timeout: 20000,
                                    content: result
                                   });
                });
      });
  },
  $scope.unsubscribe = function(company) {
        var modalOptions = {
            closeButtonText: 'Cancelar',
            actionButtonText: 'Confirmar',
            headerText: 'Confirma desuscribirse?',
            bodyText: 'Confirmar'
        };

        modalService.showModal({}, modalOptions).then(function (result) {

            SubscriptionService.unsubscribe(company).success(function (data) {
                $scope.is_subscribed = false;
            }, function (data) {
                $scope.error = data.non_field_errors[0];
            });
        });
  }
})
.controller('RegisterCtrl', function($scope, $http, $window, $location, AuthService){
      $scope.isRegister = true;
      $scope.isAuthenticated = false;
      $scope.register = function () {
                AuthService.register($scope.user)
                .then(function (data) {
                  $scope.error = 'Chequee su casilla de correo para activar la cuenta';
                }, function (data) {
                  // Erase the token if the user fails to log in
                  delete $window.sessionStorage.token;
                  $scope.isAuthenticated = false;

                  // Handle login errors here
                  $scope.error = 'Error: Problem in register';
                  $scope.welcome = '';
                });
            };
})
.controller('UserCtrl', function ($rootScope, $scope, $http, $window, $location, AuthService) {
      $scope.user = {email: '', password: ''};
      $scope.isAuthenticated = false;
      $scope.isRegister = false;
      $scope.welcome = '';
      $scope.message = '';

      $rootScope.$on('event:social-sign-in-success', function(event, userDetails){
          console.log(userDetails);
          /*
          email:"kruk.matias@gmail.com"
          imageUrl:"https://lh5.googleusercontent.com/-UY2oN9Ufqug/AAAAAAAAAAI/AAAAAAAADI4/TitelrLmlAg/s96-c/photo.jpg"
          name:"Matias Kruk"
          provider:"google"
          token:"eyJhbGciOiJSUzI1NiIsImtpZCI6IjZmNWVkZmRiODc4ZDViMTAxMWQ1MjkwNGFlMzMwZDRiNzA5NWZkNzIifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXRfaGFzaCI6IkhuZi1sN1V1UlZHdVpRVFQ3emItZVEiLCJhdWQiOiIxMzQ5MjEwOTI1MDAtMGg3Zjk2NTd2cDlvaDMwcjdyYWlta3Y2OHJjMTBjdnAuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDk5MTMwNzc4Nzk3OTM3NzIwNzgiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXpwIjoiMTM0OTIxMDkyNTAwLTBoN2Y5NjU3dnA5b2gzMHI3cmFpbWt2NjhyYzEwY3ZwLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiZW1haWwiOiJrcnVrLm1hdGlhc0BnbWFpbC5jb20iLCJpYXQiOjE0NjkwMzgyMjMsImV4cCI6MTQ2OTA0MTgyMywibmFtZSI6Ik1hdGlhcyBLcnVrIiwicGljdHVyZSI6Imh0dHBzOi8vbGg1Lmdvb2dsZXVzZXJjb250ZW50LmNvbS8tVVkyb045VWZxdWcvQUFBQUFBQUFBQUkvQUFBQUFBQUFESTQvVGl0ZWxyTG1sQWcvczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6Ik1hdGlhcyIsImZhbWlseV9uYW1lIjoiS3J1ayIsImxvY2FsZSI6ImVzIn0.HxhHM6VE3VCy1eDL2WfADxt1DpOYVLAxJbI9GK17IdtRlcO9csRfB-z14gtav57PaQhuEsqcVz7J2hhoH64ziIy4zY53qjuGTq0YVvqJw8TPjkVh2zXx3bQAl8udLDUMjGCxmSzKbbFwgJUEQYs_RGZcqNUgvpoOyrIu5aZxr3ZyiYmDN8J4SKobGVpb6VovgsmiDr7dZ5NDpUJ91jJAKiMVI7HOPRoJUZY2ZYh1ZU53-hh2pBuuTkdc4oq3Bl_qAryQI-FXfe9J9tEZPdcnvAh4XXp0nTYQLwVTuocz_mtSUwS18KiALs-4hxriUZFOuY0gXUJTrQM_XYTYXj7Aog"
          uid:"109913077879793772078"
          */
          // Guardo user-id, email
          AuthService.social_account(userDetails);
          //TODO: No actualiza el listado.
      });



      $scope.selected = 'None';
      $scope.menuOptions = [
          ['Mi cuenta', function ($itemScope) {
              $location.path("/accounts");
          }],
          ['Actualizar Contraseña', function ($itemScope) {
              $location.path("/password");
          }],
          ['Mis Puntos', function ($itemScope) {
              $location.path("/status");
          }],
          ['Vincular Redes', function ($itemScope) {
              $location.path("/social");
          }],
          ['Salir', function ($itemScope) {
              $scope.logout();
              $scope.indexClass = '';
              $location.path('/login');
          }]
      ];

      $scope.has_social = function(social) {
          var social_exists = false;
          if ($scope.profile) {
            console.log($scope.profile.social_accounts);
            $scope.profile.social_accounts.forEach(function(social_network) {
                  if (social_network.social === social) {
                      social_exists = true;
                  }
            });
            return social_exists;
          }
          return true;
      }

      $scope.submit = function () {
          AuthService.login($scope.user)
          .then(function (data) {
            $window.sessionStorage.token = data.data.auth_token;
            $scope.isAuthenticated = true;
            $scope.indexClass = 'home';
            $location.path("/");
            location.reload();
          }, function (data) {
            // Erase the token if the user fails to log in
            // delete $window.sessionStorage.token;
            $scope.isAuthenticated = false;
            $scope.indexClass = '';

            // Handle login errors here
            $scope.error = dadata.non_field_errors[0];
            $scope.welcome = '';
          });
      };

      $scope.register = function () {
          AuthService.register($scope.user)
          .then(function (data, status, headers, config) {
            $location.path("/login");
          }, function (data) {
            // Erase the token if the user fails to log in
            delete $window.sessionStorage.token;
            $scope.isAuthenticated = false;

            // Handle login errors here
            $scope.error = 'Error: Invalid user or password';
            $scope.welcome = '';
          });
      };

      $scope.logout = function () {
        $scope.welcome = '';
        $scope.message = '';
        $scope.isAuthenticated = false;
        delete $window.sessionStorage.token;
      };

      $scope.callRestricted = function () {
        AuthService.me()
        .then(function (data) {
          $scope.isAuthenticated = true;
          $scope.profile = data.data;
          $scope.indexClass = 'home';
        }, function (data) {
          $scope.isAuthenticated = false;
          $scope.indexClass = '';
          $location.path("/login");
        });
      };
      $scope.callRestricted()

    })
.controller('AccountCtrl', function($scope, AuthService, ngToast) {
  $scope.update = function update(){
      AuthService.update_me($scope.profile);
      ngToast.create({
        className: 'success',
        content: 'Updated',
        dismissButton: true
      });
  }
})
.controller('PasswordCtrl', function($scope, AuthService, ngToast) {
  $scope.passwords = {};
  $scope.passwords.new_password = '';

  $scope.update = function update(){
      AuthService.update_password($scope.passwords);
      ngToast.create({
        className: 'success',
        content: 'Password Updated',
        dismissButton: true
      });
  }
})
.controller('QrCodeCtrl', function($stateParams, $scope, AuthService, $location) {
  $scope.qr_code = $stateParams.code;
  console.log($scope.qr_code);
});
