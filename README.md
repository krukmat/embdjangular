# Embjdjangular #

A worth trying to integrate: django-angularjs-embarkjs-go-ethereum as a single stack.

### What is this repository for? ###

* It's a POC to integrate those technologies.
* 0.1

Installation
============

* docker terminal:
    * docker-compose up
    * docker ps (copy the container id from there)
    * docker exec -i -t c6435e34d30f /bin/bash

* docker instance:
    * root@web:/code# ./update_angularjs_layout.sh
    * root@web:/code# python manage.py shell
    * from backend.models import *
    * user = SingleUser.objects.create_superuser('amail@mail.com','12345')
    * browser: http://docker_ip/webapp/#!/ -> use user created in the step before.


Some important commands
=======================

* Build: docker-compose build
* Run: docker-compose up
* Bash: docker exec -i -t ca5fb3bb410a /bin/bash

Who do I talk to?
=================

* Matias Kruk <kruk.matias at gmail.com>